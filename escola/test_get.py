import requests

headers = {'Authorization': 'Token 4076775fee54b72618c019828ecff95454c8e17e'}

url_base_cursos = 'http://localhost:8000/api/v2/cursos'
url_base_avaliacoes = 'http://localhost:8000/api/v2/avaliacoes'

resultado = requests.get(url=url_base_cursos, headers=headers)

# Verificiando se o endpoint está correto
assert resultado.status_code == 200

# Testando a quantidade de registros
assert resultado.json()['count'] == 5

# Testando se o título do primeiro curso está correto
assert resultado.json()[
    'results'][0]['titulo'] == 'GraphQL: Criando APIs Profissionais e Flexíveis'
