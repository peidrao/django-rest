import requests

headers = {'Authorization': 'Token 3bf9e6fe0585ef3da6710ff5eea4cd57601140f7'}

url_base_cursos = 'http://localhost:8000/api/v2/cursos/'
url_base_avaliacoes = 'http://localhost:8000/api/v2/avaliacoes/'


editar_curso = {
    "titulo": "Arquitetura de Redes 2",
    "url": "https://www.udemy.com/course/full-stack/"
}


# Buscando curso com ID 6
# curso = requests.get(url=f'{url_base_cursos}6/', headers=headers)
# print(curso.json())


resultado = requests.put(
    url=f'{url_base_cursos}6/', headers=headers, data=editar_curso)

# Testanto o código de status HTTP
#assert resultado.status_code == 200

# Testanto o título
assert resultado.json()['titulo'] == editar_curso['titulo']
