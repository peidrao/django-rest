import requests


class TesteCursos:
    headers = {'Authorization': 'Token  4076775fee54b72618c019828ecff95454c8e17e'}
    url_base_cursos = 'http://127.0.0.1:8000/api/v2/cursos'

    def test_get__cursos(self):
        cursos = requests.get(url=self.url_base_cursos, headers=self.headers)

        assert cursos.status_code == 200

    def test_get_curso(self):
        curso = requests.get(
            url=f'{self.url_base_cursos}/3', headers=self.headers)

        assert curso.status_code == 200

    def test_post_curso(self):
        novo = {
            "titulo": "Álgebra Linear com Python", "url": "https://www.udemy.com/course/algebra-linear-com-python/"
        }

        resposta = requests.post(
            url=self.url_base_cursos, headers=self.headers, data=novo)

        assert resposta.status_code == 201
        assert resposta.json()['titulo'] == novo['titulo']

    def test_put_curso(self):
        atualizado = {
            "titulo": "Álgebra Linear com C++", "url": "https://www.udemy.com/course/algebra-linear-com-cpp/"
        }

        resposta = requests.put(
            url=f'{self.url_base_cursos}/3', headers=self.headers, data=atualizado)

        assert resposta.status_code == 200

    def test_put_titulo_curso(self):
        atualizado = {
            "titulo": "Álgebra Linear com C++", "url": "https://www.udemy.com/course/algebra-linear-com-cpp/"
        }

        resposta = requests.put(
            url=f'{self.url_base_cursos}/3', headers=self.headers, data=atualizado)

        assert resposta.json()['titulo'] == atualizado['titulo']

    def test_delete_curso(self):
        resposta = requests.delete(
            url=f'{self.url_base_cursos}/3', headers=self.headers)

        assert resposta.status_code == 204 and len(resposta.text) == 0
