import requests

headers = {'Authorization': 'Token 4076775fee54b72618c019828ecff95454c8e17e'}

url_base_cursos = 'http://localhost:8000/api/v2/cursos/'
url_base_avaliacoes = 'http://localhost:8000/api/v2/avaliacoes/'


novo_curso = {
    "titulo": "Master Full-Stack Web Development | Node, SQL, React, & More ",
    "url": "https://www.udemy.com/course/full-stack/"
}


resultado = requests.post(
    url=url_base_cursos, headers=headers, data=novo_curso)

# Testando código de status HTTP 201
assert resultado.status_code == 201

# Testando se o título do curso retornando é o mesmo informado
#assert resultado.json()['titulo'] == novo_curso['titulo']
